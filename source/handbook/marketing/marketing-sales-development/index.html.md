---
layout: markdown_page
title: "Marketing & Sales Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is Marketing & Sales Development Handbook

The Marketing & Sales Development organization includes the Sales Development, Content Marketing, Field Marketing, Online Marketing, and Marketing Operations teams. The teams in this organization employ a variety of marketing and sales crafts in service of our current and future customers with the belief that providing our audiences value will in turn grow GitLab's business.

## Marketing & Sales Development Handbooks

- [Sales Development](/handbook/marketing/marketing-sales-development/sdr/)
- [Content Marketing](/handbook/marketing/marketing-sales-development/content)
- [Field Marketing](/handbook/marketing/marketing-sales-development/field-marketing/)
- [Online Marketing](/handbook/marketing/marketing-sales-development/online-marketing/)
- [Marketing Operations](/handbook/marketing/marketing-sales-development/marketing-operations/)


## References 

- [Marketing & Sales Development Roles, Process, and 2H 2017 Plan](https://docs.google.com/presentation/d/1NBsqRudh5ELNFHRJCjt5yxLNRrigv16qC0-_sWWAcpU/edit#slide=id.g23e881c565_0_136)
- [Business Operations](/handbook/business-ops)
- [Reseller Handbook](/handbook/resellers/)
