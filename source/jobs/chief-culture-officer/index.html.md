---
layout: job_page
title: "VP of People Operations"
---

The GitLab team has grown quickly, and plans to continue grow while maintaining our culture and [remote working](https://about.gitlab.com/2015/04/08/the-remote-manifesto/), embracing development, and building scalable people-oriented policies. The VP of People Operations will be the a strategic leader and consultant that will oversee HR, Talent, and Learning & Development functions. In this role you'll be empowered to grow teams, making data driven decisions, and delivering transparent and people first programs.

## Responsibilities

- Provide strategy and oversight of the administration of compensation, benefits, HRIS     system
- Work to refine and improve our global compensation frameworks including the [Global Compensation Calculator](https://about.gitlab.com/handbook/people-operations/global-compensation/)
- Advise the executive team on relevant compliance and employment law and introduce best practices to protect the organization
- Develop and implement effective training programs for GitLab employees and community
- Harness the existing culture and values when building out internal people related policies while devising new ways to reinforce and improve upon the culture
- Manage talent initiatives from recruiting and onboarding to employee retention and offboarding
- Identify and measure organizational KPIs and OKRs for all of People Operations
- Implement global program for evaluating performance and providing feedback
- Advise company on all legal issues related to employment
- Evaluate and manage all third party People Ops vendors and partners
- Manage and grow a people operations team
- Report directly to the CEO


## Requirements

- 5+ years experience in a People Operations or HR executive position
- Recent experience in a People Ops function within a fast growing organization, preferably a tech/software company
- International HR or People Ops experience
- Enthusiasm for and broad experience with software tools
- Proven experience quickly learning new software tools
- Deep understanding and competence around US employment law and best practices, international employment law preferred
- Willingness to work with git and GitLab, using GitLab workflows within the People Ops team
- Commitment to making People Operations as open and transparent as possible
- Desire to work for a fast-moving startup and the ability to iterate quickly
- You share our [values](/handbook/values), and work in accordance with those values.
- Excellent written and verbal communication skills
- Successful completion of a [background check](/handbook/people-operations/#background-checks)

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

