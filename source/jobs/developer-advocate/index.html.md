---
layout: job_page
title: "Developer Advocate"
---

As a GitLab Developer Advocate, your goal is to talk with developers to better understand our community and see how we can help make their GitLab experience better. You’ll be traveling to events, creating high-quality code complete tutorials to help our community collaborate more effectively, work to gather product feedback from the field, and doing everything in your power to keep GitLab in the minds of the developer community. We think [this](http://www.leggetter.co.uk/2016/02/03/defining-developer-relations.html) is a great overview of developer relations.

## Responsibilities

* Create developer tutorials and content tools to help our community go from idea to launch.
* Work closely with the marketing team to identify and create events that are relevant for the GitLab community.
* You don’t mind hanging out at a university hackathon for the weekend.
* Speak to developers all around the world (in person or virtual) who are interested in using GitLab.
* Recognize trends in feedback from the community and relay that information back to our engineering and PM team.
* Put together technical presentations that keep attendees engaged and interested.
* Get on calls with our sales team and our customers to help solve a complex problem.

## Requirements for Applicants

* Background in C.S. and understanding of Git required.
* Knowledgeable and experienced with Ruby on Rails is preferred.
* You are outgoing and love crowds. You don’t mind being the center of attention but are also a really good listener.
* Is your college degree in french foreign politics with a minor in interpretive dance but you’ve been hacking together websites since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* Excellent spoken and written English
* Must love travel and hotels/Airbnbs.
* You share our [values](/handbook/values), and work in accordance with those values.

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
