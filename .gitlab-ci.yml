image: dev.gitlab.org:5005/gitlab/gitlab-build-images:www-gitlab-com

variables:
  GIT_DEPTH: "10"
  # Speed up middleman
  NO_CONTRACTS: "true"

.install: &install
  bundle install --jobs 4 --path vendor

before_script: [*install]

cache:
  key: "web"
  paths:
    - vendor

stages:
  - prepare
  - build
  - deploy

lint:
  stage: prepare
  script:
    - bundle exec rake lint
  tags:
    - gitlab-org

eslint:
  stage: prepare
  script:
    - yarn install
    - yarn run eslint
  tags:
    - gitlab-org

crop_pictures:
  before_script: []
  stage: prepare
  image: jujhars13/docker-imagemagick
  script:
    - bin/crop-team-pictures
  artifacts:
    paths:
      - data/team.yml
      - data/pets.yml
      - source/images/team/
  tags:
    - gitlab-org

rubocop:
  stage: prepare
  script:
    - bundle exec rubocop
  tags:
    - gitlab-org

rspec:
  stage: prepare
  script:
    - bundle exec rspec
  tags:
    - gitlab-org

check_links:
  before_script: []
  image: coala/base
  stage: prepare
  script:
    - git fetch --unshallow && git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*" && git fetch origin master
    - git diff --numstat origin/master..$CI_COMMIT_REF_NAME -- | awk '/(.+\.md)|(.+\.haml)/ { print $3 }' > new_files
    - coala --no-config --ci --bears InvalidLinkBear --settings follow_redirects=True --files="$(paste -s -d, new_files)"
  when: manual
  allow_failure: true
  except:
    - master
  tags:
    - gitlab-org

.build_base: &build_base
  before_script:
    - find source/images/team -type f ! -name '*-crop.jpg' -delete
    - *install
  stage: build
  dependencies:
    - crop_pictures
  artifacts:
    expire_in: 7 days
    paths:
      - public/
      - source/images/team/
  tags:
    - gitlab-org

build_branch:
  <<: *build_base
  script:
    - bundle exec rake build
  except:
    - master

build_master:
  <<: *build_base
  variables:
    MIDDLEMAN_ENV: 'production'
  script:
    - bundle exec rake build pdfs
  only:
    - master

review:
  stage: deploy
  allow_failure: true
  before_script: []
  cache: {}
  dependencies:
    - build_branch
  variables:
    GIT_STRATEGY: none
  script:
    - rsync -avz --delete public ~/pages/$CI_COMMIT_REF_SLUG
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: http://$CI_COMMIT_REF_SLUG.about.gitlab.com
    on_stop: review_stop
  only:
    - branches@gitlab-com/www-gitlab-com
  except:
    - master@gitlab-com/www-gitlab-com
  tags:
    - deploy
    - review-apps

review_stop:
  stage: deploy
  before_script: []
  artifacts: {}
  cache: {}
  dependencies: []
  variables:
    GIT_STRATEGY: none
  script:
    - rm -rf public ~/pages/$CI_COMMIT_REF_SLUG
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  only:
    - branches@gitlab-com/www-gitlab-com
  except:
    - master@gitlab-com/www-gitlab-com
  tags:
    - deploy
    - review-apps

deploy:
  stage: deploy
  cache: {}
  variables:
    GIT_STRATEGY: none
  dependencies:
    - build_master
  before_script: []
  script:
    - rsync --delete -avz public/ ~/public/
  environment:
    name: production
    url: https://about.gitlab.com
  tags:
    - deploy
  only:
    - master@gitlab-com/www-gitlab-com
